## 4.初始化后端项目

- [安装客户端工具 Robo 3T](https://robomongo.org/download)
- [安装 api 测试工具 Insomnia Core](https://insomnia.rest/download/#windows)

目录结构

```js
.
├── package.json
├── src
│   └── index.ts
└── tsconfig.json
```

### 4.1 生成项目

```js
mkdir server
cd server
cnpm init -y
```

### 4.2 安装依赖

```js
cnpm i express mongoose body-parser bcryptjs jsonwebtoken morgan cors validator helmet dotenv multer http-status-codes -S
cnpm i typescript  @types/node @types/express @types/mongoose @types/bcryptjs @types/jsonwebtoken  @types/morgan @types/cors @types/validator ts-node-dev nodemon  @types/helmet @types/multer cross-env -D
```

| 模块名              | 英文                                                                                                                                                                                                              | 中文                                                                                                                                                         |
| :------------------ | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| express             | Fast, unopinionated, minimalist web framework for node.                                                                                                                                                           | 基于 Node.js 平台，快速、开放、极简的 Web 开发框架                                                                                                           |
| @types/express      | This package contains type definitions for Express                                                                                                                                                                | express 的类型声明                                                                                                                                           |
| mongoose            | Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment. Mongoose supports both promises and callbacks.                                                                        | Mongoose 为模型提供了一种直接的，基于 scheme 结构去定义你的数据模型。它内置数据验证， 查询构建，业务逻辑钩子等，开箱即用                                     |
| @types/mongoose     | This package contains type definitions for Mongoose                                                                                                                                                               | mongoose 的类型声明                                                                                                                                          |
| body-parser         | Node.js body parsing middleware.                                                                                                                                                                                  | body-parser 是一个 HTTP 请求体解析中间件，使用这个模块可以解析 JSON、Raw、文本、URL-encoded 格式的请求体，Express 框架中就是使用这个模块做为请求体解析中间件 |
| bcryptjs            | Optimized bcrypt in JavaScript with zero dependencies. Compatible to the C++ bcrypt binding on node.js and also working in the browser.                                                                           | bcryptjs 是一个第三方加密库，用来实现在 Node 环境下的 bcrypt 加密                                                                                            |
| @types/bcryptjs     |                                                                                                                                                                                                                   |
| jsonwebtoken        | An implementation of JSON Web Tokens                                                                                                                                                                              | JSON Web Token（JWT）是一个非常轻巧的规范。这个规范允许我们使用 JWT 在用户和服务器之间传递安全可靠的信息                                                     |
| @types/jsonwebtoken | This package contains type definitions for jsonwebtoken                                                                                                                                                           | jsonwebtoken 的类型声明                                                                                                                                      |
| morgan              | HTTP request logger middleware for node.js                                                                                                                                                                        | morgan 是 express 默认的日志中间件，也可以脱离 express，作为 node.js 的日志组件单独使用                                                                      |
| @types/morgan       | This package contains type definitions for morgan                                                                                                                                                                 | morgan 的类型声明                                                                                                                                            |
| cors                | CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.                                                                                        | CORS 是用于提供 Connect / Express 中间件的 node.js 程序包，可用于启用具有各种选项的 CORS                                                                     |
| @types/cors         | This package contains type definitions for cors                                                                                                                                                                   | cors 的类型声明                                                                                                                                              |
| validator           | A library of string validators and sanitizers                                                                                                                                                                     | 一个用于字符串验证和净化的库                                                                                                                                 |
| @types/validator    | This package contains type definitions for validator.js                                                                                                                                                           | validator 的类型声明                                                                                                                                         |
| helmet              | Helmet helps you secure your Express apps by setting various HTTP headers. It's not a silver bullet, but it can help!                                                                                             | Helmet 可通过设置各种 HTTP 标头来帮助您保护 Express 应用程序                                                                                                 |
| @types/helmet       | This package contains type definitions for helmet                                                                                                                                                                 | helmet 的类型声明                                                                                                                                            |
| dotenv              | Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env. Storing configuration in the environment separate from code is based on The Twelve-Factor App methodology. | Dotenv 是一个零依赖模块，可将环境变量从.env 文件加载到 process.env 中                                                                                        |
| multer              | Multer is a node.js middleware for handling multipart/form-data, which is primarily used for uploading files. It is written on top of busboy for maximum efficiency.                                              | Multer 是用于处理`multyparty/formdata`类型请求体的 node.js 中间件，主要用于上传文件。 它是在 busboy 之上编写的，以实现最大效率。                             |
| @types/multer       | This package contains type definitions for multer                                                                                                                                                                 | multer 的类型声明                                                                                                                                            |
| typescript          | TypeScript is a language for application-scale JavaScript                                                                                                                                                         | ypeScript 是用于应用程序级 JavaScript 的语言                                                                                                                 |
| @types/node         | This package contains type definitions for Node.js                                                                                                                                                                | 该软件包包含 Node.js 的类型定义                                                                                                                              |
| ts-node-dev         | Tweaked version of node-dev that uses ts-node under the hood.                                                                                                                                                     | 调整后的版本，在后台使用 ts-node                                                                                                                             |
| nodemon             | nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.                                                 | nodemon 是一种工具，可在检测到目录中的文件更改时通过自动重新启动应用程序来帮助开发基于 node.js 的应用程序。                                                  |

### 4.3 初始化 tsconfig.json

- 执行以下命令并选择`node`

```json
npx tsconfig.json
```

### 4.4 package.json

```diff
+  "scripts": {
+    "build": "tsc",
+    "start": "cross-env PORT=8000  ts-node-dev --respawn src/index.ts",
+    "dev": "cross-env PORT=8000 nodemon --exec ts-node --files src/index.ts"
+  }
```

### 4.5 .gitignore

```js
node_modules
src/public/upload/
.env
```

### 4.6 .env

```js
JWT_SECRET_KEY=zhufeng
MONGODB_URL=mongodb://localhost/zhufengketang
```

### 4.7 src\index.ts

```js
```

### 4.8 测试

```js
npm run build
```

## 5.用户管理

- 本章的是编写用户管理相关的接口
- 本章需要编写以下接口
  - 用户注册
  - 用户登录
  - 验证用户是否登录
  - 上传头像

 目录结构
```js
.
├── package.json
├── src
│   ├── controller
│   │   └── user.ts
│   ├── exceptions
│   │   └── HttpException.ts
│   ├── index.ts
│   ├── middlewares
│   │   └── errorMiddleware.ts
│   ├── models
│   │   ├── index.ts
│   │   └── user.ts
│   ├── public
│   ├── typings
│   │   ├── express.d.ts
│   │   └── jwt.ts
│   └── utils
│       └── validator.ts
└── tsconfig.json
```

本章效果

![userinterface](http://img.zhufengpeixun.cn/userinterface.gif)

#### 5.1 src/index.ts

src/index.ts

```js
import express, { Express, Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
import HttpException from "./exceptions/HttpException";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import errorMiddleware from "./middlewares/errorMiddleware";
import * as userController from "./controller/user";
import "dotenv/config";
import multer from "multer";
import path from "path";
const storage = multer.diskStorage({
  destination: path.join(__dirname, "public", "uploads"),
  filename(_req: Request, file: Express.Multer.File, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});
const upload = multer({ storage });
const app: Express = express();
app.use(morgan("dev"));
app.use(cors());
app.use(helmet());
app.use(express.static(path.resolve(__dirname, "public")));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.get("/", (_req: Request, res: Response) => {
  res.json({ success: true, message: "hello world" });
});
app.get("/user/validate", userController.validate);
app.post("/user/register", userController.register);
app.post("/user/login", userController.login);
app.post(
  "/user/uploadAvatar",
  upload.single("avatar"),
  userController.uploadAvatar
);
app.use((_req: Request, _res: Response, next: NextFunction) => {
  const error: HttpException = new HttpException(404, "Route not found");
  next(error);
});
app.use(errorMiddleware);
const PORT: number = (process.env.PORT && parseInt(process.env.PORT)) || 8000;
(async function () {
  mongoose.set("useNewUrlParser", true);
  mongoose.set("useUnifiedTopology", true);
  await mongoose.connect("mongodb://localhost/zhufengketang");
  app.listen(PORT, () => {
    console.log(`Running on http://localhost:${PORT}`);
  });
})();
```

#### 5.2 src/exceptions/HttpException.ts

src/exceptions/HttpException.ts

```js
class HttpException extends Error {
    constructor(public status: number, public message: string, public errors?: any) {
        super(message);
    }
}
export default HttpException;
```

#### 5.3 src/middlewares/errorMiddleware.ts

src/middlewares/errorMiddleware.ts

```js
import HttpException from "../exceptions/HttpException";
import { Request, Response, NextFunction } from "express";
import { INTERNAL_SERVER_ERROR } from "http-status-codes";
const errorMiddleware = (
  error: HttpException,
  _request: Request,
  response: Response,
  _next: NextFunction
) => {
  response.status(error.status || INTERNAL_SERVER_ERROR).send({
    success: false,
    message: error.message,
    errors: error.errors,
  });
};
export default errorMiddleware;
```

#### 5.4 src/utils/validator.ts

src/utils/validator.ts

```js
import validator from "validator";
import { IUserDocument } from "../models/user";

export interface RegisterInput extends Partial<IUserDocument> {
  confirmPassword?: string;
}

export interface RegisterInputValidateResult {
  errors: RegisterInput;
  valid: boolean;
}

export const validateRegisterInput = (
  username: string,
  password: string,
  confirmPassword: string,
  email: string
): RegisterInputValidateResult => {
  let errors: RegisterInput = {};
  if (username == undefined || validator.isEmpty(username)) {
    errors.username = "用户名不能为空";
  }
  if (password == undefined || validator.isEmpty(password)) {
    errors.password = "密码不能为空";
  }
  if (confirmPassword == undefined || validator.isEmpty(confirmPassword)) {
    errors.password = "确认密码不能为空";
  }
  if (!validator.equals(password, confirmPassword)) {
    errors.confirmPassword = "确认密码和密码不相等";
  }
  if (email == undefined || validator.isEmpty(password)) {
    errors.email = "邮箱不能为空";
  }
  if (!validator.isEmail(email)) {
    errors.email = "邮箱格式必须合法";
  }
  return { errors, valid: Object.keys(errors).length == 0 };
};
```

#### 5.5 src/typings/jwt.ts

src/typings/jwt.ts

```js
import { IUserDocument } from "../models/user";

export interface UserPayload {
    id: IUserDocument['_id']
}
```

#### 5.6 src\models\index.ts

src\models\index.ts

```js
export * from "./user";
```

#### 5.7 src/models/user.ts

src/models/user.ts

```js
import mongoose, { Schema, Model, Document, HookNextFunction } from 'mongoose';
import validator from 'validator';
import jwt from 'jsonwebtoken';
import { UserPayload } from '../typings/jwt';
import bcrypt from 'bcryptjs';
export interface IUserDocument extends Document {
    username: string,
    password: string,
    email: string;
    avatar: string;
    generateToken: () => string,
    _doc: IUserDocument
}
const UserSchema: Schema<IUserDocument> = new Schema({
    username: {
        type: String,
        required: [true, '用户名不能为空'],
        minlength: [6, '最小长度不能少于6位'],
        maxlength: [12, '最大长度不能大于12位']
    },
    password: String,
    avatar: String,
    email: {
        type: String,
        validate: {
            validator: validator.isEmail
        },
        trim: true,
    }
}, { timestamps: true ,toJSON: {
      transform: function (_doc: any, result: any) {
        result.id = result._id;
        delete result._id;
        delete result.__v;
        delete result.password;
        delete result.createdAt;
        delete result.updatedAt;
        return result;
      },
    },});

UserSchema.methods.generateToken = function (): string {
    let payload: UserPayload = ({ id: this._id });
    return jwt.sign(payload, process.env.JWT_SECRET_KEY!, { expiresIn: '1h' });
}
UserSchema.pre<IUserDocument>('save', async function (next: HookNextFunction) {
    if (!this.isModified('password')) {
        return next();
    }
    try {
        this.password = await bcrypt.hash(this.password, 10);
        next();
    } catch (error) {
        next(error);
    }
});
UserSchema.static('login', async function (this: any, username: string, password: string): Promise<IUserDocument | null> {
    let user: IUserDocument | null = await this.model('User').findOne({ username });
    if (user) {
        const matched = await bcrypt.compare(password, user.password);
        if (matched) {
            return user;
        } else {
            return null;
        }
    }
    return user;
});
interface IUserModel<T extends Document> extends Model<T> {
    login: (username: string, password: string) => IUserDocument | null
}
export const User: IUserModel<IUserDocument> = mongoose.model<IUserDocument, IUserModel<IUserDocument>>('User', UserSchema);
```

#### 5.8 src\controller\user.ts

src\controller\user.ts

```js
import { Request, Response, NextFunction } from 'express';
import { validateRegisterInput } from '../utils/validator';
import HttpException from '../exceptions/HttpException';
import { UNPROCESSABLE_ENTITY, UNAUTHORIZED } from 'http-status-codes';
import { IUserDocument, User } from '../models/user';
import { UserPayload } from '../typings/jwt';
import jwt from 'jsonwebtoken';
export const validate = async (req: Request, res: Response, next: NextFunction) => {
    const authorization = req.headers['authorization'];
    if (authorization) {
        const token = authorization.split(' ')[1];
        if (token) {
            try {
                const payload: UserPayload = jwt.verify(token, process.env.JWT_SECRET_KEY!) as UserPayload;
                const user = await User.findById(payload.id);
                if (user) {
                    delete user.password;
                    res.json({
                        success: true,
                        data: user
                    });
                } else {
                    next(new HttpException(UNAUTHORIZED, `用户不合法!`));
                }
            } catch (error) {
                next(new HttpException(UNAUTHORIZED, `token不合法!`));
            }

        } else {
            next(new HttpException(UNAUTHORIZED, `token未提供!`));
        }
    } else {
        next(new HttpException(UNAUTHORIZED, `authorization未提供!`));
    }
}
export const register = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let { username, password, confirmPassword, email, addresses } = req.body;
        const { valid, errors } = validateRegisterInput(username, password, confirmPassword, email);
        if (!valid) {
            throw new HttpException(UNPROCESSABLE_ENTITY, `参数验证失败!`, errors);
        }
        let user: IUserDocument = new User({
            username,
            email,
            password,
            addresses
        });
        let oldUser: IUserDocument | null = await User.findOne({ username: user.username });
        if (oldUser) {
            throw new HttpException(UNPROCESSABLE_ENTITY, `用户名重复!`);
        }
        await user.save();
        let token = user.generateToken();
        res.json({
            success: true,
            data: { token }
        });
    } catch (error) {
        next(error);
    }
}

export const login = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let { username, password } = req.body;
        let user = await User.login(username, password);
        if (user) {
            let token = user.generateToken();
            res.json({
                success: true,
                data: {
                    token
                }
            });
        } else {
            throw new HttpException(UNAUTHORIZED, `登录失败`);
        }
    } catch (error) {
        next(error);
    }
}
export const uploadAvatar = async (req: Request, res: Response, _next: NextFunction) => {
    let { userId } = req.body;
    let domain = process.env.DOMAIN || `${req.protocol}://${req.headers.host}`;
    let avatar = `${domain}/uploads/${req.file.filename}`;
    await User.updateOne({ _id: userId }, { avatar });
    res.send({ success: true, data: avatar });
}
```

#### 5.9 typings\express.d.ts

src\typings\express.d.ts

```js
import { IUserDocument } from "../models/user";
declare global {
    namespace Express {
        export interface Request {
            currentUser?: IUserDocument | null;
            file: Multer.File
        }
    }
}
```

#### 5.10 .env

.env

```js
JWT_SECRET_KEY=zhufeng
MONGODB_URL=mongodb://localhost:27017/zhufengketang
PORT=6699
DOMAIN=http://api.zhufeng.cn
```

#### 5.11 package.json

```js
  "scripts": {
    "build": "tsc",
    "start": "cross-env NODE_ENV=development  ts-node-dev --respawn src/index.ts",
    "dev": "cross-env NODE_ENV=production nodemon --exec ts-node --files src/index.ts"
  }
```
